import React, {useState, useEffect} from 'react';
import {render} from 'react-dom';
import MapGL from 'react-map-gl';
import {AmbientLight, PointLight, LightingEffect} from '@deck.gl/core';
import DeckGL from '@deck.gl/react';
import {PolygonLayer, ScatterplotLayer} from '@deck.gl/layers';
import {TripsLayer} from '@deck.gl/geo-layers';
import {data} from './data'
import './App.css'

// Source data CSV
const DATA_URL = {
  BUILDINGS:
    'https://raw.githubusercontent.com/visgl/deck.gl-data/master/examples/trips/buildings.json', // eslint-disable-line
  TRIPS: data // eslint-disable-line
};

const ambientLight = new AmbientLight({
  color: [255, 255, 255],
  intensity: 0.1
});

const pointLight = new PointLight({
  color: [255, 255, 255],
  intensity: 2.0,
  position: [-74.05, 40.7, 8000]
});

const lightingEffect = new LightingEffect({ambientLight, pointLight});

const material = {
  ambient: 0.1,
  diffuse: 0.6,
  shininess: 32,
  specularColor: [60, 64, 70]
};

const DEFAULT_THEME = {
  buildingColor: [74, 80, 87],
  trailColor0: [253, 128, 93],
  trailColor1: [23, 184, 190],
  material,
  effects: [lightingEffect]
};

const INITIAL_VIEW_STATE = {
  longitude: -74,
  latitude: 40.72,
  zoom: 13,
  pitch: 45,
  bearing: 0
};

const MAP_STYLE = 'https://tiles.basemaps.cartocdn.com/gl/positron-gl-style/style.json';

const landCover = [[[-74.0, 40.7], [-74.02, 40.7], [-74.02, 40.72], [-74.0, 40.72]]];

function App({
  buildings = DATA_URL.BUILDINGS,
  trips = DATA_URL.TRIPS,
  trailLength = 1800,
  initialViewState = INITIAL_VIEW_STATE,
  mapStyle = MAP_STYLE,
  theme = DEFAULT_THEME,
  loopLength = 1800, // unit corresponds to the timestamp in source data
  animationSpeed = 1
}) {
  const [time, setTime] = useState(0);
  const [animation] = useState({});
  const [running, setRunning] = useState(false)

  let loopRunning = false;
  const animate = () => {
    if (loopRunning) { // use variable outside of closure to allow toggle
      setTime(t => (t + animationSpeed) % loopLength);
      animation.id = window.requestAnimationFrame(animate); // draw next frame
    }
  };

  useEffect(() => {
    console.log(time)
    if(time > 1750) {
      setRunning(!running)
    }
  }, [time])
  
  useEffect(() => {
    if (!running) {
      loopRunning = false;
      window.cancelAnimationFrame(animation.id);
      return;
    }

    loopRunning = true;
    animation.id = window.requestAnimationFrame(animate); // start animation
    return () => window.cancelAnimationFrame(animation.id);
  }, [running]);
    
  const layers = [
    // This is only needed when using shadow effects
    new PolygonLayer({
      id: 'ground',
      data: landCover,
      getPolygon: f => f,
      stroked: false,
      getFillColor: [0, 0, 0, 0]
    }),
    new TripsLayer({
      id: 'trips',
      data: trips,
      getPath: d => d.path,
      getTimestamps: d => d.timestamps,
      getColor: d => (d.vendor === 0 ? theme.trailColor0 : theme.trailColor1),
      opacity: 1,
      widthMinPixels: 10,
      rounded: true,
      trailLength,
      currentTime: time,
      shadowEnabled: true
    }),
    new ScatterplotLayer({
      id: 'scatterplot-layer',
      data,
      pickable: true,
      opacity: 0.8,
      stroked: true,
      filled: true,
      radiusScale: 6,
      radiusMinPixels: 1,
      radiusMaxPixels: 100,
      lineWidthMinPixels: 1,
      getPosition: d => d.path,
      getRadius: d => 20,
      getFillColor: d => [255, 140, 0],
      getLineColor: d => [0, 0, 0]
    })
    // new PolygonLayer({
    //   id: 'buildings',
    //   data: buildings,
    //   extruded: true,
    //   wireframe: false,
    //   opacity: 0.3,
    //   getPolygon: f => f.polygon,
    //   getElevation: f => f.height,
    //   getFillColor: theme.buildingColor,
    //   material: theme.material
    // })
  ];

  return (
    <>
    <DeckGL
      layers={[layers]}
      effects={theme.effects}
      initialViewState={initialViewState}
      controller={true}
      mapboxAccessToken={'pk.eyJ1IjoidmljdG9yeWFwY2EiLCJhIjoiY2t2ajQ2Z2liY3I5ODJxbnpkNW9ua2tmMiJ9.KZzS-NM7sncMffOkcsJTXw'}
    >
      <MapGL 
        reuseMaps 
        mapStyle={mapStyle} 
        preventStyleDiffing={true} 
        mapboxAccessToken={'pk.eyJ1IjoidmljdG9yeWFwY2EiLCJhIjoiY2t2ajQ2Z2liY3I5ODJxbnpkNW9ua2tmMiJ9.KZzS-NM7sncMffOkcsJTXw'}
      />
    </DeckGL>
    <button class="button-class" onClick={() => {setRunning(!running)}}>Button</button>
    <div class="slider" style={{ width: '100%', marginTop: "1.5rem" }}>
        <input
          style={{ width: '100%' }}
          type="range"
          min="0"
          max="1800"
          step="0.1"
          value={time}
          onChange={(e) => { setTime(Number(e.target.value)); }}
        />
      </div>
    </>
  );
}


export default App;
